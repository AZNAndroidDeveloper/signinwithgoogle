package uz.azn.googlesignin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import uz.azn.googlesignin.databinding.FragmentDetailBinding

class DetailFragment : Fragment(R.layout.fragment_detail) {
    private lateinit var binding: FragmentDetailBinding
    private lateinit var firebaseAuth: FirebaseAuth
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    binding = FragmentDetailBinding.bind(view)
        firebaseAuth = FirebaseAuth.getInstance()
        validateUser()
        binding.btnLogout.setOnClickListener {
            firebaseAuth.signOut()
        }
    }
    private fun validateUser(){
        val firebaseUser = firebaseAuth.currentUser
        if (firebaseUser ==  null){
            fragmentManager!!.beginTransaction().replace(R.id.frame_layout,IntroFragment()).commit()
        }else{
            val userId = firebaseUser.uid
            val email = firebaseUser.email
            with(binding){
                tvEmail.text = email
                tvUserId.text = userId
            }
        }
    }

}
