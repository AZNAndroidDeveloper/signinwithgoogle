package uz.azn.googlesignin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import uz.azn.googlesignin.databinding.FragmentIntroBinding

class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var firebaseAuth: FirebaseAuth

    companion object {
        private val SIGN_IN_CODE = 102
        private val TAG = "GOOGLE SIGN IN"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build()
        googleSignInClient = GoogleSignIn.getClient(requireContext(), gso)
        firebaseAuth = FirebaseAuth.getInstance()
        with(binding) {
            signInButton.setOnClickListener {
                validateUser()
                Log.d(TAG, "Sign in appeared ")
                val intent = googleSignInClient.signInIntent /// bu oynani ochib beradi
                startActivityForResult(intent, SIGN_IN_CODE)

            }
        }

    }

    // bu userni bor yoqligni tekshiradi
    private fun validateUser() {
        val firebaseUser = firebaseAuth.currentUser
        if (firebaseUser != null) {
            // User logged in
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SIGN_IN_CODE) {
            Log.d(TAG, "Sign in appeared ")
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                firebaseGoogleAuth(account)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun firebaseGoogleAuth(account: GoogleSignInAccount?) {
        // sign in qilish uchun kerak boladi
        val credential = GoogleAuthProvider.getCredential(account!!.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnSuccessListener { result ->
            // yani bu undagi malumotlatni olsa boladi
            val firebaseUser = firebaseAuth.currentUser

            if (result.additionalUserInfo!!.isNewUser) {
                // foydalanuvchi yoq bolsa  yaratiladi
                Toast.makeText(requireContext(), "New user created", Toast.LENGTH_SHORT).show()

            } else {
                 // foydalanuvchi bor bolsa kiradi
                Toast.makeText(requireContext(), "User already exists", Toast.LENGTH_SHORT).show()
            }

            fragmentManager!!.beginTransaction().replace(R.id.frame_layout, DetailFragment())
                .commit()

        }.addOnFailureListener { error ->
            Toast.makeText(requireContext(), "Failed - ${error.message}", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onStart() {
        super.onStart()
        val user = firebaseAuth.currentUser
        if (user != null) {
            fragmentManager!!.beginTransaction().replace(R.id.frame_layout, DetailFragment())
                .commit()
        }
    }
}